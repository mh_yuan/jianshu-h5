import { injectGlobal } from 'styled-components';

injectGlobal `
@font-face {
    font-family: "iconfont";
    src: url('./iconfont.eot?t=1539505696522');
    /* IE9*/
    src: url('./iconfont.eot?t=1539505696522#iefix') format('embedded-opentype'),
        /* IE6-IE8 */
        url('data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAABXkAAsAAAAAICgAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADMAAABCsP6z7U9TLzIAAAE8AAAARAAAAFY8jEjGY21hcAAAAYAAAAEwAAADeE49e3FnbHlmAAACsAAAECoAABZUfG0XM2hlYWQAABLcAAAAMQAAADYVzoIoaGhlYQAAExAAAAAgAAAAJAq8BpxobXR4AAATMAAAABkAAAB0dwH//GxvY2EAABNMAAAAPAAAADxECEhabWF4cAAAE4gAAAAdAAAAIAExAZpuYW1lAAATqAAAAUUAAAJtPlT+fXBvc3QAABTwAAAA9AAAAWji1/l2eJxjYGRgYOBikGPQYWB0cfMJYeBgYGGAAJAMY05meiJQDMoDyrGAaQ4gZoOIAgCKIwNPAHicY2BkkWKcwMDKwMHUyXSGgYGhH0IzvmYwYuRgYGBiYGVmwAoC0lxTGByeMTzbydzwv4EhhrmRYRpQmBEkBwD0HwzNeJztk01OAkEQhb+B4U9HQVEBheCChARcGWOMuCacA7ZeghtwHa/iAd418HUXiUsvYFe+SXfnZWqqXg3QAOrmyZRQqyi8o2j6tsj3dc7yfcm3zzMevSuFOqrUU19DTTTVXEuttNZGO+110NfxCFbVrOpaNdA4qxYn1fZX9ecqnHXGs+PF8cpbjvdTfOSAO0bcWzuiy5gb+pzTdn1XXLuGJpf0XEmLBy6oMWBIh1vXUuVq/1eVHsXn6TRJ7gXurH0M3F1UD9xtVAZpVtQIsqYZ2AvUCuwKagf2xxMU2ClUBfYMdQOSthekOVU/sKNoENhbNAzSHGsckPJPAjuPpgHpffMgzbUWAekbl4EnBK0CzwpaB6QebAJS/m1Ayr8LSDXtA08XOgTp39JXQPUDFydxGHicvVh7lNzUedd375U0I81IMyONNO+HNCvtc3Z2HprF613bu35AscEuBowdAhQwEGgcCLhg4uAklBJaYhrgcNpQYgqHhMYQB8dtCSfGYFqgBw4NrttQGqBA25ByAHMaQtrdcb+rWTuP0/7X01mtdB/f1f2ev++7EkAQju+kz5EjgiUIUNUgnTStKUhLlt3qVptBN1kHunv+rUylkrmGpIbyu+G5TAWgQl6GSmZhb6ZyDm2y3ZUM2ZipCIJA8H1/SreSHwgN4SJ8Y2ClNfAblqyBbEmy5Pl1kCc80p2BxSuwbD5h2SXgpG1Owcfr4OKa/oWzbsApcMJHpgLPtRbv9FSSl6pElqI1Anq6+dBk61G3PJgtb/Xclf5W06xu06BkaUP1okuylUy2uyyRdwHymVRpU+2Kanl9Ll3N0dLdo407vJQI1BGJRuhwhHiUlkCA/NBojmRLpSqQ0VS5oEVtpsNkUS4lzMvK6y5gon/xWNu1QZK63nD29ESOUt2fXFUrDbMtc8XPG3FdtN9VqRFR06Y+SPK5XI4oTq0CZVSXQFFfn6LPko+FAWGpcJogDATeOCqlTnRARaA9bFQAqqhMuPzBMtIMBqRwjpN1fRxcxhVUBtEyZVFyvHZ3IGjSQxJVds6KkUw58fQ3tXIhQb4VV6W4oh+eZmrMLUZvOJrM9XqFxA3TLK098y0L6MNqIf70LGPRnQt/L8vHBTmlyYD3F7Ry/KHDiXJGFlfviFI9lzi6M1pyYoo4eVhTYpKqPUITHtiPPK2l2eTOZOHojghja56JF9SHNskpeV6K81voG9fSUfK2sFxYwX1jGQRdWQoNrIONDep7PgoVTvBxnUhygDbnfW9G7loaqoV8zjSqSVUVWcRS1NjFGxJGYmhsMlssDsU0v7bCKXnDtfPq1vqqA5JopauMMKZ7S9eed/75IxA3supVUJNERWQjKcXytIRW0jU9lreBMWXWzM6uuHZpZ3Sy/oV1tduCJcm8vWr8VIWKheaWT13xW6dP0OInr9o2thRtJ6I8n0XbvS2khILgocdPoFQSk0tgp1EMqxXUul7Np3VoB03LlOTW/9CaIMKh44IIF26cmay3J58CQRSPC0+9knOctuPk3sJny3Gyb2bDZ468xV77xkUPBI8MDhRUbH7jNdabgipOtarw68++fx2iL9ApISoYAkaoWE1Wu/iflA3fc1DtGEYQeg34UAbwyX8u/DZRRhfuJ296h0txN14+vxyDXk5OxWW4q9xbA5vh8WKW3Dm4UCbnupAvxmOlT5Tjbm9K0lIy3FrubdhM4dslgWPL4/Rf6BphGPf1Z6BTBx+NjeppolERCCQXRzDWcUTrO7HVDNr0RweHhsVjBw58yNiHBy7cFJWiKWXd2fcfZezo/XuOijAyJIH0YsTQC9/v7lp54BhjSHyMDTPDkNgAO7pnDyfds2JHSzbkF2TNFDgvqIznya1ClttIB+5roadB0LShBBya4L82btijWkVPkymFiAaX1BoDcIVl5HLDKZI47+yH4/kYRCQ/KctxuGFoqPeldD2TzqQFIYKybkdfuFYwhSGUtyW0hWlhRljT9wiTg5xtBRitLZS3XUOf8D1JnuGBfFID4M4A9wnHG3D73sFbJ8aI8BT6yagfD6LZ3V7l092n5qO943/VurJcuzXC/Hh1BMR/3/edn4jiT3q3LIdat4YXCMsHggG8AFYsDpHP0ue/tvS6mn1TPvGbTEuy5//4jpcXtoChshVSZFeudkX39u9S9v0v3bkXVuX52oH8rzxO4NbN9FL6OZQ8Iwwibp1gk1ZRlqQGbtXx/OQMIIjaVbR9EpGd5xf4ARRGCnjdB183sgBZo3dR+Pw8jDmk6tTrzsLrTt21i8WxUomKkDMXPjZzOZNEzNz8c24DoOHSM/pPgfF8wwhVMAJriKDTghAFz5EROTkwJvnmzaCTtK1WEz3dhw5XrJ0eMEfA7+CABkWw0Q07ydARkvQ/epflVxd6l6s0mVXjZG6I3OyNxRRFVyNU6UWNrAEwa7yHu080x+FooginR3MKrIXHYDad7h2M5tI0ovR2DHegPQq/G5Vjai7am9gvybK0v1KBx4rjhIwXHwMwVXgccLj3896pJ3R6mD5BB4URYVw4Gz0HQ8RFLSKb3gigu0g8VXo4JvXTNGKMPcBzaDcAN9kSQywVkzy6OoEbTHEef5FepyDs0PvMunJPqSrVSL40whRXNyjcU5bVR1VdV/epUSLCg2ecaSuqsXDajTeSbaqamhnvbWFi+jAhVJeluGRBSQGmLAn+SDKlKMCzaezSZXc324Rppnv9F9UI6GovoyYQggmR/vrqZu3U3ibYv8prnXdAJrCXkHv3xEg8NnLjRCIrp5JUkiuLmPqHTKDbBBUrkhzGUgcHEZ6CLvqVKbloWB3ClOB3bfx3eOrAfhvTJpYqHFb7lDJsVjVNJb/PpZq/ITeRArrh4SZ52f0hPPrK3+SbxuwPV5r4y5oT73AaeJzTfzph6QC6rcNn2vs2UkhP5uHI6Kuw99WvmubsP84aze4ELjHnnwI9pMSb0I//Hj1ECaIszwNLhClhpbBKOAOtiDbB+EeOLJ4TwtKGOr7nOtxa3SA0FsJA5xcwYHIY9BzJdkvw6y0iHJwXJ+paRvzisk2XY1ucP3jBWQhIam6mgLqmlKjDI+ydR7/9Y1H8ce/PGoWhfH6o8HYDKxls5N+eWGxQYEe+vuK6RuRmm0aaNXbkvj1HFqarJGqnmalo+VElER+7/JTdTzL25O47noRpk6/Kp3/lccJvb6Jr6S5BRywoCIKBkiyigFxFfBO5eRzJtHxMPAdgZQseaq0EfPY28+fp+VotqNX+Dl7vVWmkPzP/Qf85A26nVuu4f0HuDvPJPukNulxIoE9wFflhqEsmFoYgXWO2vNT8B2bLnD+W8loGeXPa8Fsm1Q1j/pjZ9CBF3grf8QbayUXfCi1zMiXhZTUXa9I6kGemttZqW6eu/AqFeu6sY6bu6MaHZ+XGCP0KdSFoNAKg91638hJv+5EY/o5s9y5Zed29oS7+ld6OPKqCLaB6oJ/b+ijf3yncwJDtLhy6bT+l+2/r3x94hbFXHgjvF7deHKPCyZnb9s+dmMH7woOtZ9v/33rfhTG5i3t5OiUMSER26QWwAA8++fGXDx3aBUvJNwt/3tMyvUOcL47Ju+hjSN8Ia9pzUAuIYrKFJZ59Mtsjf1i+okoWawDEpq53MhXSUEl904CngVlaNAw4YacfRuTdkd/IvxoBSB8pnNPauI2QbRvP2kaBvA/ZpU6/jxT/FAXymbP63dYswnerOQcw11OxEsIr7MBPK0EFr3AePnJKO/OlxtD1I+OAi/pLVeVWYlj9Ls4Xiv3hjdsW/gHmmuFb8E48KE8unyz3e7C0PDmDHd7muoHjC8fvwnx1leByXBvHuqNOugaGPwo00OVJFCv5EkFUMyxUQyqZev/D1LCTZM2Duhov2yBvV2rDteh2iKQrcSVxsMESzpXR669XaHYwDeQ7iWEM7cYHhm0bH9Rpfngosb/Xs/x8H1t30Y/RLuPCJcJOYZfwZRx0nRFANEIOpqF94lhGuk2b5wodWvx81j+GYdGEp6+W3WmGBTrPO94Ez0x16PfxLTyhphcPbTLlZU/LaokjxPGp53e8GWK3AjkZ5jHJtzt+0253kq5t0BysXr16zZrBgbIDYMRG1ptKZGKgUJhM67IiHbLaRj6WHDJlQ5Fi9UdWr4Yo1oBmzBxMEZDl5IdGhkkSiyIMFuUI5hxZzm5pMoLVWt5avaZ3Kys11jKLUZCy1eQFmwB/kma4xFH0rbJ1EwJ5rO3u++qdpqN4l6yonT0ByerpS69pliFmVZZsfmxjbiITLxTWDw4yqo0Ob9y3Ka4SAkUjn82MXjgxmhABNJZp2GdeO8rE3nhcARXWr4VPLFse1Z08+ui5vRfPPTdSTRhneD6B8zbAKbf0HhAWz8z30HfoOsSLQBBsh1emYWXYwQOExWwsIB1eLWJq6OcSbhhePCKVfSIWrja1SsyMaKTmi2C+9BKYoth796UzTo3NleqrYPv+d8VKNaklYzIYM9nJtdnCiiqOie9ermQ2n5JdEmv/zvQTPxfFnz/xvY+YCQXNLtCrxZfva108mlvbbuSTLKfmchExwl6+70/+tu/LxxEXgH5BcLDzS0UXL00wm/F6owgtXn5ZM4BCL8lOZ3qzcbArmtxw3gfHvjrj/NRp6FWbxOiVWD1tilVyRJV7q53GzzKOk/lZw4EDccg56iKuHKXfpZ/EGj4njApjuKsbeIvfCLDU6/I6c4ZDi3wKVF0NbAMHUT1+Mhyi1703eWW8jpb5mkqXW3Bpzl2eJ6zNFn5vNxYwJZIjNxoRIBUC7320Q6ENZddDtDfoFkFcJ722l43qhGRJr2ZhxZMk8HpODY8VShhXhzGuqFDGk+2dwveEl4Q3QYGlcDuiMoIvhkin7bnciJLs9N0fLReGkMGTv8wHuCgl0rVNe/FbxzKwuCM0JgIen5wslNVG2rRsdgM8tqTtltnqWkGn1cGo7MOnZXMUwasA9oTVtbodnPe4s1jTePYKfNrmRzHPDff18D/8KMO3CNXZx9pwz3FoD4dbotMFnTYOdhvhKkwryCM6YytUuM9TJ6525WHwHdkNxSuD2ZpIc1/l4nQCu+13QjH4FRLpxMe6zQ2/AGBTQq45aQguzYB6UhrFwb3Sls059dsOtjibuKjtcrjhUIQHWdnkEYHcYDhIabMVagHzeZsT4K0b+BOBP01vmd9BkpouM5pu2hQoAzmh6RF1aq4tEWUVY/od07PJGKgRiRBZR0NHcgNXNTLlCAZ7RsbBmOpnynNVe0hqxhkAAYL1MIcTGV9GFEON04iTqOClkf4EE5EKqzKK1IRsoFGRD4JmYjGdlOKyqkQwKEVSThTXtBttSTbuipYlIBTUaDSVp0wHchVhJGkT3AWPpiRTHlFScXypyF8ZkfFkDnwrIiFHURFdOLJcJW9ANJuW0wljzAr5i2XrQd4kMDiXkFnEiKJEMTz+/sGK9WeW5yxC/xI5laRhZJZBqUhE5DcxYMmcWy0WjcqiSLi8SIS7KNkYI8ZQCrnEfDuQBZo2kCGJ7pq/ESxzKIvUMYgiNS2OZfKlaoU5DsZooVReYzFYPjqGfCeTOj9twGitYXjpblkBEE9hpelCphyNxSixbHLZCJ4W+J/I4lKcy4FMJJR0vKTrhhQDKuNBXYXFCfypikhDtVPo/RuqJcb4cl2iXEPYYjTK0AzDl120nMmr6kNnDuISFEE19YKBZqogImSwn5Ap6ier4dmy9c9jW5Bd1Bjr76GkEM9hdGwzgTLDTH8O9lqXNgw3zr+AQCpZmZ4qOSQ5MWRnxi8XCUMsAD9NICEa6HUBspHyU6h8PA8G3TmsuGbnSlO5SpWzTYhhxiPIbCSaSCZRAlqpkPJ0JegCdCeDaVouM7SwYZ6otQ4hJp72v2Fi6/8OE1/56IYonYguYqK0TvrRXnoSEhUgyOsbCIn/DcVXpKgAAHicY2BkYGAA4o+ftifF89t8ZeBmYQCB6y/lFWD0/7//s9geMDcCuRwMTCBRAHiIDYQAAAB4nGNgZGBgbvjfwBDDzvD/7/9/bA8YgCIoQBYAsZwHZHicY2FgYGChALPjlPv/F4n9H1kOAGnABHIAAAAAAAAAACgAwAE2AZ4B8AIuAnQCpAMcA2ADxgRIBKQFIgVWBXgFqAXeBhIGKga4BvYHwggeCFYIogrgCyp4nGNgZGBgkGXsY+BiAAEmIOYCs/+D+QwAGjcBzQAAAHicZY9NTsMwEIVf+gekEqqoYIfkBWIBKP0Rq25YVGr3XXTfpk6bKokjx63UA3AejsAJOALcgDvwSCebNpbH37x5Y08A3OAHHo7fLfeRPVwyO3INF7gXrlN/EG6QX4SbaONVuEX9TdjHM6bCbXRheYPXuGL2hHdhDx18CNdwjU/hOvUv4Qb5W7iJO/wKt9Dx6sI+5l5XuI1HL/bHVi+cXqnlQcWhySKTOb+CmV7vkoWt0uqca1vEJlODoF9JU51pW91T7NdD5yIVWZOqCas6SYzKrdnq0AUb5/JRrxeJHoQm5Vhj/rbGAo5xBYUlDowxQhhkiMro6DtVZvSvsUPCXntWPc3ndFsU1P9zhQEC9M9cU7qy0nk6T4E9XxtSdXQrbsuelDSRXs1JErJCXta2VELqATZlV44RelzRiT8oZ0j/AAlabsgAAAB4nG2Ny1KDQBBFuSiPxATi+xn/IItx49o/yB9YAwzQEXpMmLEMXx8CpMqFd9NVt0+fdlxnyNT5P0u4OMM5PPgIEGKCKS4wwxwRYixwiStc4wa3uMM9HvCIJzzjBUu8OjGlmnPNphu1YhNkJLmV7GVyp9irpU4o+CYuKsuRkUlizfHinfXOb7RtrJ7lkktLWcckdpGW8kuyIdVSV+bWbTZ+U2q7V+FoFuGoE15P9OsNzf9qRHgsU8lFMDwRwUAJf7W1VFVhrvi38xWTEyjiLXFLH+u1sQlJvfJ7u/D3qu7A0/e30GjbX0Zj8ylTQz/KcQ46ZmPg') format('woff'),
        url('./iconfont.ttf?t=1539505696522') format('truetype'),
        /* chrome, firefox, opera, Safari, Android, iOS 4.2+*/
        url('./iconfont.svg?t=1539505696522#iconfont') format('svg');
    /* iOS 4.1- */
}

.iconfont {
    font-family: "iconfont" !important;
    font-size: 16px;
    font-style: normal;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
/* 
.icon-iconfontconment:before {
    content: "\e604";
}

.icon-dianzan:before {
    content: "\e646";
}

.icon-daren:before {
    content: "\e618";
}

.icon-maobi:before {
    content: "\e690";
}

.icon-pinglun:before {
    content: "\e610";
}

.icon-tabbuticon7nor:before {
    content: "\e61c";
}

.icon-sousuo:before {
    content: "\e62a";
}

.icon-fanhuidingbu:before {
    content: "\e60f";
}

.icon-chakantiezihuifu:before {
    content: "\e663";
}

.icon-sj:before {
    content: "\e627";
}

.icon-shouye:before {
    content: "\e60c";
}

.icon-dianzan1:before {
    content: "\e6b9";
}

.icon-pinglun1:before {
    content: "\e63b";
}

.icon-huifu:before {
    content: "\e61d";
}

.icon-shouji:before {
    content: "\e606";
}

.icon-fanhuidingbu1:before {
    content: "\e61f";
}

.icon-shoucang:before {
    content: "\e613";
}

.icon-sousuo1:before {
    content: "\e617";
}

.icon-shouji1:before {
    content: "\e609";
}

.icon--quill:before {
    content: "\e608";
}

.icon-fenxiang:before {
    content: "\e681";
}

.icon-shoucang1:before {
    content: "\e600";
}

.icon-qinziAPPtubiao-:before {
    content: "\e648";
}

.icon-huifu1:before {
    content: "\e662";
}

.icon-yemian:before {
    content: "\e602";
}

.icon-dianzan2:before {
    content: "\e603";
}

.icon-yemian-copy:before {
    content: "\e605";
}

.icon-touxiang:before {
    content: "\e62b";
}

.icon-dianzan_active:before {
    content: "\e607";
} */
`