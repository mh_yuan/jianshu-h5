import React from 'react';
import { CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import {
  HeaderWrapper,
  Logo,
  Nav,
  NavItem,
  SearchWrapper,
  SearchInfo,
  SearchInfoTitle,
  SearchInfoList,
  SearchInfoItem,
  NavSearch,
  Addition,
  Button,
  SearchInfoSwitch,
} from './style';
import { actionCreators } from './store/index';
// import { toJS } from 'immutable';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.getListArea = this.getListArea.bind(this);
    }
    render() {
        const { handleInputFocus, handleInputBlur, focused, searchList } = this.props;
        
        return ( 
          <HeaderWrapper >
            <Logo href='/' />
            <Nav>
                <NavItem className='left'>
                <i className='iconfont'>&#xe605;</i>
                首页</NavItem>
                <NavItem className="left">下载App</NavItem>
                <SearchWrapper>
                    <CSSTransition
                        timeout={200}
                        in={focused}
                        classNames="slide"
                    >
                        <NavSearch
                            onFocus={ () => handleInputFocus(searchList.size)}
                            onBlur={handleInputBlur}
                            className={ focused ? 'focused' : ''}>
                        </NavSearch>
                    </CSSTransition>
                    <i className={focused ? 'iconfont focused' : 'iconfont'}>&#xe617;</i>
             
                        {
                            this.getListArea(focused)
                        }
                </SearchWrapper>
                <NavItem className='right login'>登录</NavItem>
                <NavItem className='right mode'>Aa</NavItem>
            </Nav>
            <Addition>
                <Button className='writing'>写文章</Button>
                <Button className='reg'>注册</Button>
            </Addition>
          </HeaderWrapper>
        )
    }

    getListArea() {
        const { focused, mouseIn, searchList, searchPageMark, totalSearchPageMark, changeSearchPage, mouseEnterHandle, mouseLeaveHandle } = this.props;
        const list = searchList.toJS();
        const pageList = [];

        if(list.length > 0) {
            for(let i = (searchPageMark - 1 ) * 10; i < searchPageMark * 10; i++) {
                pageList.push( 
                    <SearchInfoItem key={list[i]} >{list[i]}</SearchInfoItem> 
                )
            }
        }
        
        if(focused || mouseIn) {
            return (
                <SearchInfo 
                    onMouseEnter={mouseEnterHandle}
                    onMouseLeave={mouseLeaveHandle}
                    >
                    <SearchInfoTitle>热门搜索
                        <SearchInfoSwitch onClick={ () => changeSearchPage(searchPageMark, totalSearchPageMark) }>换一批</SearchInfoSwitch>
                    </SearchInfoTitle>
                    <SearchInfoList> { pageList  } </SearchInfoList>
                </SearchInfo>
            )
        } else {
            return;
        }
    }
}



const mapStateToProps = (state) =>  {
  return {
    // focused: state.header.focused // 不使用immutable时的写法
    // focused: state.get('header').get('focused') //使用immutable时的写法
    focused: state.getIn(['header', 'focused']), //使用redux-immutable时的简写
    mouseIn: state.getIn(['header', 'mouseIn']),
    searchList: state.getIn(['header', 'searchList']),
    searchPageMark: state.getIn(['header', 'searchPageMark']),
    totalSearchPageMark: state.getIn(['header', 'totalSearchPageMark']),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    mouseEnterHandle() {
        dispatch(actionCreators.setMouseInAction(true));
    },
    mouseLeaveHandle() {
        dispatch(actionCreators.setMouseInAction(false));
    },
    changeSearchPage(mark, totalMark) {
        let nextMark = mark < totalMark ? (mark + 1) : 1;
        dispatch(actionCreators.changeSearchPageAction(nextMark));
    },
    handleInputFocus(count) {        
        dispatch(actionCreators.searchFocusAction());
        if (count === 0) {
            dispatch(actionCreators.getSearchList());
        }
    },
    handleInputBlur() {
        const action = actionCreators.searchBlurAction();
        dispatch(action);
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
