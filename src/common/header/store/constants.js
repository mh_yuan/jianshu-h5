export const SEARCH_FOCUS = 'header/SEARCH_FOCUS';
export const SEARCH_BLUR = 'header/SEARCH_BLUR';
export const GET_SEARCH_LIST = 'header/GET_SEARCH_LIST';
export const SET_SEARCH_LIST = 'header/SET_SEARCH_LIST';
export const SET_MOUSE_IN = 'header/SET_MOUSE_IN';
export const CHANGE_SEARCH_PAGE = 'header/CHANGE_SEARCH_PAGE';