import * as actionTypes from './constants';
import axios from 'axios';
import { fromJS } from 'immutable';

export const getSearchList = () => {
    return (dispatch) => {
        axios.get('/api/searchList.json').then((res) => {
            let count = Math.ceil(res.data.length/10);            
            dispatch(setSearchList(res.data, count));
        }).catch((e) => {
            console.log(e);
        })
    }
}

export const searchFocusAction = () => ({
    type: actionTypes.SEARCH_FOCUS
})

export const searchBlurAction = () => ({
    type: actionTypes.SEARCH_BLUR
})

export const setSearchList = (list, count) => ({
    type: actionTypes.SET_SEARCH_LIST,
    list: fromJS(list),
    marks: count
})

export const setMouseInAction = (mouseIn) => ({
    type: actionTypes.SET_MOUSE_IN,
    mouseIn: mouseIn
})

export const changeSearchPageAction = (searchPageMark) => ({
    type: actionTypes.CHANGE_SEARCH_PAGE,
    searchPageMark
})
