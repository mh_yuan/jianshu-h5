import * as contants from './constants';
import { fromJS } from 'immutable';
const headerList = ["行距杯2018征文", "区块链", "小程序", "vue", "毕业", "PHP", "故事", "flutter", "理财", "美食", "投稿", "手帐", "书法", "PPT", "穿搭", "打碗碗花", "简书", "姥姥的澎湖湾", "设计", "创业", "交友", "籽盐", "教育", "思维导图", "疯哥哥", "梅西", "时间管理", "golang", "连载", "自律", "职场", "考研", "慢世人", "悦欣", "一纸vr", "spring", "eos", "足球", "程序员", "林露含", "彩铅", "金融", "木风杂谈", "日更", "成长", "外婆是方言", "docker"];

const defaultState = fromJS({
    focused: false,
    mouseIn: false,
    searchList: [],
    totalSearchPageMark: 1,
    searchPageMark: 1,

})

export default (state = defaultState, action) => {
    if (action.type === contants.SEARCH_FOCUS) {
        return state.set('focused', true);
    }
    
    if (action.type === contants.SEARCH_BLUR) {
        return state.set('focused', false);
    }

    if(action.type === contants.GET_SEARCH_LIST) {
        return state.merge({
            searchList: action.list,
            totalSearchPageMark: Math.ceil(action.list/10)
        });
    }

    if(action.type === contants.SET_SEARCH_LIST) {
        return state.merge({
            searchList: action.list,
            totalSearchPageMark: action.marks
        });
    }

    if(action.type === contants.SET_MOUSE_IN) {
        return state.set('mouseIn', action.mouseIn);
    }

    if(action.type === contants.CHANGE_SEARCH_PAGE) {
        return state.set('searchPageMark', action.searchPageMark);
    }

    return state;
}
