import styled from 'styled-components';
import logoPic from '../../statics/top_logo.png';

export const HeaderWrapper = styled.div`
    position: relative;
    width: 100%;
    height: 58px;
    box-sizing: border-box;
    border-bottom: 1px solid #f0f0f0;
    &.wrapper{
        display: block;
        max-width: 1440px;
        min-width: 768px;
        height: 100%;
        margin: 0 auto;
    }
`
export const Logo = styled.a.attrs({
    href: '../../statics/top_logo.png'
})`
    position: absolute;
    left: calc(50% - 720px);
    display: block;
    width: 100px;
    height: 56px;
    background-color: #fff;
    background-image: url(${logoPic});
    background-size: 100% 100%;
    background-repeat: no-repeat;
    background-position: center;
    cursor: pointer;
`
export const Nav = styled.div`
    max-width: 960px;
    min-width: 768px;
    height: 100%;
    margin: 0 auto;
    background-color: #fff;
`

export const NavItem = styled.div`
    width: 89px;
    height: 56px;
    line-height: 56px;
    ${'' /* padding: 0 15px; */}
    margin-right: 10px;
    font-size: 17px;
    color: #333;
    cursor: pointer;
    &.left {
        float: left;
    }
    &.right {
        float: right;
        color: #969696;
    }
    &.active {
        color: #969696;
    }
    &.mode {
        width: 49px;
        height: 55px;
    }
    &.login {
        width: 56px;
        height: 35px;
    }
`
export const SearchWrapper = styled.div`
    position: relative;
    display: block;
    float: left;
    height: 38px;
    outline: none;
    border-radius: 20px;
    margin-top: 8px;
    font-size: 15px;
    line-height: 38px;
    background-color: #eee;
    padding: 0px 15px;
    box-sizing: border-box;
    .iconfont {
        position: absolute;
        right: 5px;
        bottom: 4px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
        line-height: 30px;
        border-radius: 15px;
        text-align: center;
        &.focused {
            background-color: #777;
            color: #fff;
        }
    }
`
export const NavSearch = styled.input.attrs({
    placeholder: '搜索'
})`
    display: inline-block;
    width: 160px;
    height: 38px;
    margin: 0 0 0 5px;
    padding: 0 20px 0 5px;
    background-color: #eee;
    border: 1px solid #eee;
    font-size: 14px;
    color: #666;
    border: none;
    outline: none;
    &.focused {
        width: 240px !important;
    }
    &&::placeholder {
        color: #999;
    }
    &.slide-enter {
        width: 160px;
        transition: all .2s  esse-out;
    }
    &.slide-enter-active {
        width: 240px;
    }
    &.slide-exit {
        transition: all .2s  esse-out;
    }
    &.slide-exit-active {
        width: 160px;
    }
    
`
export const SearchInfo = styled.div`
    display: block;
    position: absolute;
    left: 0;
    top: 56px;
    width: 240px;
    padding: 0 20px 10px;
    background-color: #fff;
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.2);
    overflow: hidden;
`
export const SearchInfoTitle = styled.div`
    display: inline-block;
    margin-top: 20px;
    margin-bottom: 15px;
    line-height: 20px;
    font-size: 14px;
    color: #969696;

`
export const SearchInfoSwitch = styled.span`
    position: absolute;
    right: 20px;
    top: 20px;
    display: block;
    font-size: 13px;
    cursor: pointer;
`
export const SearchInfoList = styled.div `
    display: block;
    padding: 5px;
`
export const SearchInfoItem = styled.a`
    display: block;
    float: left;
    font-size: 12px;
    margin: 5px 10px 10px 0;
    ${'' /* margin-right: 10px;
    margin-bottom: 15px; */}
    line-height: 20px;
    padding: 0 5px;
    border: 1px solid #ccc;
    color: #777;
    border-radius: 3px;

`
export const Addition = styled.div`
    position:absolute;
    display: inline-block;
    right: 150px;
    top: 0;
    width: 200px;
    height: 50px;

`

export const Button = styled.div`
    width: 90px;
    height: 45px;
    line-height: 45px;
    text-align: center;
    font-size: 15px;
    display: inline-block;
    box-sizing: border-box;
    border-radius: 30px;
    &.writing {
        width: 100px;
        float: right;
        height: 40px;
        line-height: 40px;
        margin: 8px 0 0 10px;
        border-radius: 20px;
        font-size: 15px;
        color: #fff;
        background-color: #ea6f5a;
    }
    &.reg{ 
        float: right;
        width: 80px;
        height: 38px;
        margin: 9px 5px 0 1px;
        line-height: 40px;
        border: 1px solid rgba(236,97,73,.7);
        border-radius: 20px;
        font-size: 15px;
        color: #ea6f5a;
        background-color: transparent;
    }
`
